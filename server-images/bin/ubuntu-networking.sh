#!/bin/bash

#
# script: ubuntu-networking.sh
# author: jorge.medina@kronops.com.mx
# desc: Update netplan config

# Enable debug mode and log to file
export DEBUG=1
LOG=/var/log/ubuntu-networking.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# Stop on errors
#set -e

echo "Create netplan config file with eth0 and dhcp"
cat <<EOF >/etc/netplan/01-netcfg.yaml;
network:
  version: 2
  ethernets:
    eth0:
      dhcp4: true
EOF

# Change predictable network interface names for eth0
sed -i 's/GRUB_CMDLINE_LINUX="\(.*\)"/GRUB_CMDLINE_LINUX="net.ifnames=0 biosdevname=0 \1"/g' /etc/default/grub;
update-grub;

