# -*- coding: UTF-8 -*-

import os
import requests
from time import sleep as t

from unittest import TestCase

from functions import functions as f

class Openurl(TestCase):
    '''
    Test Case that launches a browser then load an url from params
    '''
    def test_app(s):
        url = 'http://%s' %  os.getenv('url', '127.0.0.1')
        s.wd.get(url)
        screenA = f.getScreenshot(s.wd, 'screenA.png')
        req = requests.get(url)
        loadStatus = True if req.status_code == 200 else False
        s.output.append([
          f.blue('Open url'),
          "Url: %s" % (f.green(url)),
          f.green('OK') if loadStatus else f.red('FAIL')
          ])
        if loadStatus is False: f.quitExecution(s.wd)
