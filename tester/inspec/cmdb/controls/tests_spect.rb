# encoding: utf-8

ip = command('hostname')

control ip.stdout  do
  title 'Check for services'
  describe service('apache2') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  describe service('mysql') do
    puts ip.stdout
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
  title 'LAMP instalation'
  describe package('apache2') do
    it { should be_installed }
  end
  describe package('apache2-bin') do
    puts ip.stdout
    it { should be_installed }
  end
  describe package('apache2-bin') do
    it { should be_installed }
  end
  describe package('libapache2-mod-php7.0') do
    it { should be_installed }
  end
  describe package('php7.0') do
    it { should be_installed }
  end
  # describe package('php7.0-mysql') do
#    it { should be_installed }
#  end
  describe package('mysql-server-5.7') do
    it { should be_installed }
  end
  describe package('mysql-client-5.7') do
    it { should be_installed }
  end
  describe package('mysql-server-core-5.7') do
    it { should be_installed }
  end
  describe port(3306) do
    it { should be_listening }
    its('processes') { should include 'mysqld' }
    its('protocols') { should include 'tcp' }
    its('addresses') { should include '127.0.0.1' }
  end
  describe port(80) do
    it { should be_listening }
    its('processes') { should include 'apache2' }
    its('protocols') { should include 'tcp' }
    its('addresses') { should include '::' }
  end
end
