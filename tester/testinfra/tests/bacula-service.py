#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from unittest import TestCase
from defs import functions as f

class Bacula-Service(TestCase):
    def test_pkgs(s):
        packages = ['bacula-server','bacula-director-mysql','bacula-console']
        f.test_pkgs(s.host, packages, s.output)

    def test_services(s):
        services = ['bacula-director','bacula-sd']
        f.test_service_running(s.host, services, s.output)
        f.test_service_enabled(s.host, services, s.output)

    def test_file_exist(s):
        files = [
            '/etc/bacula',
            '/var/lib/bacula',
            '/etc/bacula/bconsole.conf'
        ]
        f.test_file_exist(s.host, files, s.output)
