bacula-client
=============

Tasks for setting up bacula client.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

hostname_fqdn: "{{inventory_hostname}}"
hostname_name: "{{hostname_fqdn.split('.').0}}"

backup_dir: backup-dir
backup_dir_pwd: BACKUPDIRPWD
backup_mon: backup-mon
backup_mon_pwd: BACKUPMONPWD

dbbackup_user: root
dbbackup_pass: s3cr3t!!!

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - bacula-client

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
