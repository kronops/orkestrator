jenkins-service
===============

Tasks for setting up jenkins slave within swarm plugin.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

template_file: swarm.service.j2
jenkins_home: /var/lib/jenkins
swarm_file: swarm-client-3.9.jar
jenkins_user: admin
jenkins_pass: admin
user: jenkins

Dependencies
------------

swarm plugin installed on Master.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
        - swarm-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: axel.herrera@kronops.com.mx.
