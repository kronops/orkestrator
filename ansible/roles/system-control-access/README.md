system-control-access
=====================

Tasks for setting up local and remote system control access.

Requirements
------------

Ubuntu based system.

Role Variables
--------------

useradmin_uid: 1000
useradmin_comment: 'Linux Server Administrator'

usersupport_uid: 1004
usersupport_comment: 'Linux Server Support'

Dependencies
------------

Not at the moment.

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - system-control-access

License
-------

MIT

Author Information
------------------

Please any question, please contact the autor at: jorge.medina@kronops.com.mx.
